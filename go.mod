module gitee.com/teval/curl

go 1.19

require (
	gitee.com/teval/goz v1.0.9
	github.com/gin-gonic/gin v1.7.3
)

require (
	github.com/basgys/goxml2json v1.1.0 // indirect
	github.com/idoubi/goutils v1.0.5 // indirect
	github.com/tidwall/gjson v1.14.1 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	golang.org/x/net v0.0.0-20220624214902-1bab6f366d9e // indirect
	golang.org/x/text v0.3.7 // indirect
)
